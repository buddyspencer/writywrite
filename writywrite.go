package main

import (
	"bufio"
	"fmt"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"time"
)

var (
	wrfile = "data/file.log"
	count = kingpin.Arg("count", "How many lines per 50 microseconds").Int()
)

func main() {
	kingpin.Parse()
	if *count == 0 {
		*count = 1
	}
	if _, err := os.Stat(wrfile); os.IsNotExist(err) {
		f, err := os.Create(wrfile)
		if err != nil {
			panic(err)
		}
		f.Close()
	}
	f, err := os.OpenFile(wrfile, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		panic(err)
	}
	f.Sync()
	wf := bufio.NewWriter(f)

	for {
		t := time.Now()
		for x := 0; x < *count; x++ {
			_, err := wf.WriteString(fmt.Sprintf("TEST[%s] THIS IS A TEST MESSAGE\n", t.Format("2006-01-02")))
			if err != nil {
				panic(err)
			}
		}
		time.Sleep(50 * time.Microsecond)
	}
}
